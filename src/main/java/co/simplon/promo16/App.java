package co.simplon.promo16;

import java.util.ArrayList;
import java.util.List;
import co.simplon.promo16.oop.Calculator;
import co.simplon.promo16.oop.Dog;
import co.simplon.promo16.oop.Food;
import co.simplon.promo16.oop.Looper;
import co.simplon.promo16.oop.NameOrganizer;
import co.simplon.promo16.oop.Person;
import co.simplon.promo16.oop.Validator;

/**
 * Hello world!
 *
 */
public class App {
  public static void main(String[] args) {
    NameOrganizer prenomClasse = new NameOrganizer();
    prenomClasse.getNameStartingBy("n");
    prenomClasse.StudentS();
    System.out.println(prenomClasse.promoMaker());

    List<String> stringList = new ArrayList<>();
    stringList.add("Ga");
    stringList.add("Zo");
    stringList.add("Bu");
    stringList.add("Meu");

    for (int i = 0; i < stringList.size(); i++) {
      System.out.println(stringList.get(i));
    }

    Looper loop = new Looper();
    loop.love(12);
    Looper table2 = new Looper();
    table2.multyTable(2);
    table2.multiTables();
    Looper pyramide = new Looper();
    pyramide.pyramide(10, '^');

    Calculator calcul = new Calculator();
    calcul.add(5, 2);
    calcul.divide(5, 2);
    calcul.multiply(5, 2);
    calcul.substract(5, 2);

    Calculator casio = new Calculator();
    int result = casio.calculate(5, 12, "+");
    System.out.println(result);
    System.out.println(calcul.isEven(5));

    Validator toutOk = new Validator();
    toutOk.minlength = 2;
    toutOk.maxlength = 32;
    String phrase1 = "miamMiam";
    System.out.println(toutOk.validate(phrase1));

    Person person1 = new Person("dupond", "paul", 30);
    Person person2 = new Person("dupont", "paul", 40);
    System.out.println(person1);
    person1.sleep();
    System.out.println(person1);
    System.out.println(person2);
    person2.sleep();
    System.out.println(person2);
    person1.socialize(person2);
    Food patate = new Food("patate", 50);
    person1.eat(patate);
    // conditionExplanation ();

    // }
    // private static void conditionExplanation () {
    // int age = 25 ;
    // if (age >= 18) {
    // System.out.println("18 ans ou plus");
    // }

    // int ageLimite = 50;
    // if (age < ageLimite) {
    // System.out.println("c'est ok");

    // String prénom = "nolwenn";
    // if (prénom == "nolwenn") {
    // System.out.println("bonjour," + prénom);

    // } else {
    // System.out.println ("casse toi!");
    // }
    // if (prénom.length() == age ) {
    // System.out.println("ton prénom a le même nombre de caractère que ton age");

    // }
    // int nombre = 13;
    // if (nombre % 2==0) {
    // System.out.println("even");
    // } else{
    // System.out.println("odd");

    // }
    // }
    // }
    Dog goodDog = new Dog("naza", "corggi", 3);

    System.out.println("hello my name is " + goodDog.name + " i am a " + goodDog.breed + " and i am " + goodDog.age
        + " years old. Wouaf Wouaf");
  }
}
