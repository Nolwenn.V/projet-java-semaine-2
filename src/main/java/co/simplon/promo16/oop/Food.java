package co.simplon.promo16.oop;

public class Food {
    public String name;
    public int calories;

    public Food(String name, int calories) {
        this.name = name;
        this.calories = calories;
    }

}
