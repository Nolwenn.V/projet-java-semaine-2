package co.simplon.promo16.oop;

public class Calculator {
    public void add(int a, int b) {

        System.out.println(a + b);
    }

    public void multiply(int a, int b) {

        System.out.println(a * b);

    }

    public void substract(int a, int b) {

        System.out.println(a - b);

    }

    public void divide(int a, int b) {

        System.out.println(a / b);

    }

    public Integer calculate(int a, int b, String operator) {

        if (operator == "+") {
            return a + b;
        }
        if (operator == "-") {
            return a - b;
        }
        if (operator == "*") {
            return a * b;
        }
        if (operator == "/") {
            return a / b;
        }

        return null;

    }

    public boolean isEven(int a) {
        if (a % 2 == 0) {
            return true;
        } else {
            return false;
        }

    }

}