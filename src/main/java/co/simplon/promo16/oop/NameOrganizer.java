package co.simplon.promo16.oop;

import java.util.ArrayList;
import java.util.List;

public class NameOrganizer {
    private List<String> prenomList = new ArrayList<>(
            List.of("Yousra", "Phonevilay", "Florian", "Roland", "Melissa", "Florent", "Magatte", "Melwic", "Aïcha",
                    "Oualid", "Claire", "Kevin", "Khaled", "Rafaël", "Nolwenn"));

    public void getNameStartingBy(String lettre) {
        List<String> affichageList = new ArrayList<>(List.of());
        for (String prenom : prenomList) {
            if (prenom.startsWith(lettre.toUpperCase())) {
                affichageList.add(prenom);
            }

        }

        System.out.println(affichageList);
    }

    public void oddOnly() {
        for (int i = 1; i < prenomList.size(); i += 2) {
            System.out.println(prenomList.get(i));
        }

    }

    public void StudentS() {
        for (int i = 0; i < prenomList.size(); i++) {
            if (i == 0) {
                System.out.println(prenomList.get(i).toUpperCase());
            } else if (i == prenomList.size() - 1) {
                System.out.println(prenomList.get(i).toUpperCase());
            } else {
                System.out.println(prenomList.get(i));
            }
        }

    }

    public List<Person> promoMaker() {
        List<Person> promo = new ArrayList<>();
        for (String firstName : prenomList) {
            Person person = new Person("P16", firstName, 80);
            promo.add(person);
        }

        return promo;
    }
}
