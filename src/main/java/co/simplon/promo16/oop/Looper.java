package co.simplon.promo16.oop;

public class Looper {
    public void love(int num) {
        String str = "i love Java";

        for (int i = 0; i < num; i++) {
            str = str.concat(" very");
        }
        str = str.concat(" much");
        System.out.println(str);
    }

    public void multyTable(int table) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i * table);
        }
    }

    public void multiTables() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }

    public void pyramide(int etage, char symbole) {
        for (int i = 0; i < etage * 2; i += 2) {
            for (int j = etage * 2 - i; j > 1; j--) {
                System.out.print(" ");
            }
            for (int j = 0; j <= i; j++) {
                System.out.print(symbole + " ");
            }
            System.out.println();
        }

    }
}
