package co.simplon.promo16.oop;

public class Person {
    private String name;
    private String firstName;
    private int energy = 100;

    public Person(String name, String firstName, int energy) {
        this.name = name;
        this.firstName = firstName;
        this.energy = energy;
    }

    public String toString() {
        return "This is a Person. name: " + name + ", firstName: " + firstName + " and energy " + energy;
    }

    public void sleep() {
        if (energy <= 60) {
            energy += 40;
        }
    }

    public void socialize(Person person) {
        if (person.energy > 10) {
            System.out.println("Hello " + firstName + " how are you? ");
        }
        energy -= 10;
        if (person.energy < 30) {
            System.out.println("I can't even ");
        }
        if (person.energy > 40 && person.energy <= 70) {
            System.out.println("I'm ok ");
        }
        if (person.energy > 70) {
            System.out.println("I'm fine ");
        }
    }

    public void answer(String question) {
        System.out.println();

    }

    public void eat(Food food) {
        System.out.println(firstName + " eat " + food.name);
        energy += food.calories / 10;
        System.out.println(energy);

    }
}
