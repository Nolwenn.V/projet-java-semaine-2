package co.simplon.promo16.oop;

public class Validator {
    public int minlength;
    public int maxlength;

    public boolean isCorrectLength(String str) {
        if (str.length() >= minlength && str.length() <= maxlength) {
            return true;
        } else
            return false;

    }

    public boolean haveTrailingSpaces(String str) {
        if (str.startsWith(" ") || str.endsWith(" ")) {
            return true;
        } else
            return false;
    }

    public boolean haveSpecialChars(String str) {
        if (str.contains("-") || str.contains("_") || str.contains("!")) {
            return true;
        } else
            return false;

    }

    public boolean validate(String str) {
        return isCorrectLength(str) && !haveTrailingSpaces(str) && !haveSpecialChars(str);

    }
}
